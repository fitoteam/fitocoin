package fitocoin;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

/**
 * @author Tiago Stapenhorst Martins
 * @author Felipe Ramon de Lara
 */
public class Encryption {

    public static byte[] asymmetricDecryptMessage(PublicKey pk, byte[] encryptedMessage) {
        try {
            Cipher cipher = Cipher.getInstance(Constants.ASYMMETRIC_CYPHER_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, pk);
            return cipher.doFinal(encryptedMessage);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(Message.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static byte[] asymmetricDecryptMessage(PrivateKey pk, byte[] encryptedMessage) {
        try {
            Cipher cipher = Cipher.getInstance(Constants.ASYMMETRIC_CYPHER_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, pk);
            return cipher.doFinal(encryptedMessage);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(Message.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static byte[] asymmetricEncryptMessage(PublicKey pk, byte[] decryptedMessage) {
        try {
            Cipher cipher = Cipher.getInstance(Constants.ASYMMETRIC_CYPHER_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, pk);
            return cipher.doFinal(decryptedMessage);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(Message.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static byte[] asymmetricEncryptMessage(PrivateKey pk, byte[] decryptedMessage) {
        try {
            Cipher cipher = Cipher.getInstance(Constants.ASYMMETRIC_CYPHER_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, pk);
            return cipher.doFinal(decryptedMessage);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(Message.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
     public static byte[] symmetricEncrypt(byte[] data, SecretKey secKey) {
        try {
            Cipher c = Cipher.getInstance(Constants.SYMMETRIC_CYPHER_ALGORITHM);
            c.init(Cipher.ENCRYPT_MODE, secKey);
            byte[] encryptedData = c.doFinal(data);
            return encryptedData;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(Encryption.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static byte[] symmetricDecrypt(byte[] encryptedData, SecretKey secKey){
        try {
            Cipher c = Cipher.getInstance(Constants.SYMMETRIC_CYPHER_ALGORITHM);
            c.init(Cipher.DECRYPT_MODE, secKey);
            byte[] data = c.doFinal(encryptedData);
            return data;
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(Encryption.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
