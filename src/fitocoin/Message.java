package fitocoin;

import fitocoin.Constants.MessageType;

/**
 * @author Tiago Stapenhorst Martins
 * @author Felipe Ramon de Lara
 */
class Message implements java.io.Serializable {

    private MessageType type; //indica o tipo de objeto contido na decrypted_message
    private EntityBean origin;
    //private String destiny; 
    private byte[] binaryObject;
    private byte[] encryptedSecretKey;
    //private byte[] decryptedMessage;
    //private Object objectMessage;

    public Message(MessageType type, EntityBean origin, byte[] binaryObject) {
        this.type = type;
        this.origin = origin;
        this.binaryObject = binaryObject;
    }
    
    public Message(MessageType mt, EntityBean origin){
        this.origin = origin;
        this.type = mt;
        this.binaryObject = null;
    }
    
    public void printEntityBean() {
        System.out.println("Type: " + this.type + "\n" + this.origin.getEntityBean() + "\n");
    }
    
    public String getEntityBeanString() {
        return "Type: " + this.type + "\n From: " + this.origin.getName();
    }

    public byte[] getEncryptedSecretKey() {
        return encryptedSecretKey;
    }

    public void setEncryptedSecretKey(byte[] encryptedSecretKey) {
        this.encryptedSecretKey = encryptedSecretKey;
    }
    
    

//    public EntityBean deserializeToEntityBean() {
//        return (EntityBean) SerializationUtils.deserialize(this.decryptedMessage);
//    }
//
//    public Payment[] deserializeToDatabase() {
//        //Recebe o banco de dados EntityPool e o preenche.
//        return (Payment[]) SerializationUtils.deserialize(this.decryptedMessage);
//    }
//
//    public Payment deserializeToTransaction() {
//        return (Payment) SerializationUtils.deserialize(this.decryptedMessage);
//    }
//
//    public Payment deserializeToPaymentNotice() {
//        return (Payment) SerializationUtils.deserialize(this.decryptedMessage);
//    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public EntityBean getOrigin() {
        return origin;
    }

    public void setOrigin(EntityBean origin) {
        this.origin = origin;
    }

    public byte[] getBinaryObject() {
        return binaryObject;
    }

    public void setBinaryObject(byte[] binaryObject) {
        this.binaryObject = binaryObject;
    }
}
