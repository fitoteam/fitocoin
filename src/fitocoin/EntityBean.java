package fitocoin;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.PublicKey;

/**
 * @author Tiago Stapenhorst Martins
 * @author Felipe Ramon de Lara
 */
public class EntityBean implements Serializable {

    private PublicKey pkey;
    private String name;
    private InetAddress ip;
    private int unicastPort;

    public PublicKey getPkey() {
        return pkey;
    }

    public void setPkey(PublicKey pkey) {
        this.pkey = pkey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public InetAddress getIp() {
        return ip;
    }

    public void setIp(InetAddress ip) {
        this.ip = ip;
    }

    public void setIp(String ip) throws UnknownHostException {
        this.ip = InetAddress.getByName(ip);
    }

    public int getUnicastPort() {
        return unicastPort;
    }

    public void setUnicastPort(int unicastPort) {
        this.unicastPort = unicastPort;
    }
    
    public void printEntityBean(){
        System.out.println(this.getEntityBean());
    }
    
    public String getEntityBean() {
        return "Entity Bean\nName: " + this.name + "\nIP: "+ this.getIp().toString() + "\nUnicast port: " + this.unicastPort + "\nPublic Key: " + this.pkey.toString();
    }

}
