package fitocoin;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Tiago Stapenhorst Martins
 * @author Felipe Ramon de Lara
 */
public final class Constants {

    public static final String MULTICAST_IP = "234.0.0.1";
    public static final int MULTICAST_PORT = 6789;
    public static final InetAddress MULTICAST_ADDRESS = getMulticastAddress();
    public static final String ASYMMETRIC_CYPHER_ALGORITHM = "RSA";
    public static final String SYMMETRIC_CYPHER_ALGORITHM = "AES";
    public static final int INITIAL_MONEY = 100;
    public static final int MINING_REWARD = 1;
    public static final int BYTE_ARRAY_SIZE = 1024*16;
    public static final int CRYPTO_KEY_SIZE = 1024*2;
    
    public enum MessageType {
        HELLO, PAYMENT_RECORDS, PAYMENT_OFFER, VERIFY_PAYMENT, PAYMENT_VERIFIED, GOODBYE, RECORD_PAYMENT
    }
    
    public enum PaymentType {
        NORMAL, REWARD
    }
    
    public static final InetAddress getMulticastAddress() {
        try {
            return InetAddress.getByName(Constants.MULTICAST_IP);
        } catch (UnknownHostException ex) {
            Logger.getLogger(Constants.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}

