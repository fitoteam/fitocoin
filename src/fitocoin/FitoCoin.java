package fitocoin;

import static java.lang.Thread.sleep;
import java.util.Scanner;

/**
 * @author Tiago Stapenhorst Martins
 * @author Felipe Ramon de Lara
 */
public class FitoCoin {

    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        //rotina(); //Execução hard coded
        executarFuncao(criarUsuario()); //Execução interativa - Atenção, não criar usuários que escutam na mesma porta
        //cada usuário deve escutar em uma porta diferente.
    }

    public static void rotina() throws InterruptedException {
        Entity user1 = new Entity("João", 7848);
        Entity user2 = new Entity("Zé", 7849);
        Entity user3 = new Entity("Jorge", 7850);
        Entity user4 = new Entity("Tito", 7851);

        sleep(1000);
        System.out.println("Saldo inicial de todos.");
        user2.paymentRecords.getEntityBalance("Zé", user2.name);
        user1.paymentRecords.getEntityBalance("Zé", user1.name);
        user3.paymentRecords.getEntityBalance("Zé", user3.name);
        user4.paymentRecords.getEntityBalance("Zé", user4.name);

        user2.paymentRecords.getEntityBalance("João", user2.name);
        user1.paymentRecords.getEntityBalance("João", user1.name);
        user3.paymentRecords.getEntityBalance("João", user3.name);
        user4.paymentRecords.getEntityBalance("João", user4.name);

        user2.paymentRecords.getEntityBalance("Jorge", user2.name);
        user1.paymentRecords.getEntityBalance("Jorge", user1.name);
        user3.paymentRecords.getEntityBalance("Jorge", user3.name);
        user4.paymentRecords.getEntityBalance("Jorge", user4.name);

        /*
                System.out.println("Fazendo pagamento de Zé para João");
                user2.paymentRecords.addVerifiedPayment(new Payment(null, new java.util.Date(), user1.getBean(), user2.getBean(), null, 50, Constants.PaymentType.NORMAL));
         */

        sleep(1000);

        user1.poolData();
        user2.poolData();
        user3.poolData();

        sleep(1000);
        user1.sendPaymentOffer(10, user2.getBean());
        sleep(1000);
        user2.paymentRecords.getEntityBalance("Zé", user2.name);
        user1.paymentRecords.getEntityBalance("Zé", user1.name);
        user3.paymentRecords.getEntityBalance("Zé", user3.name);
        user4.paymentRecords.getEntityBalance("Zé", user4.name);

        user2.paymentRecords.getEntityBalance("João", user2.name);
        user1.paymentRecords.getEntityBalance("João", user1.name);
        user3.paymentRecords.getEntityBalance("João", user3.name);
        user4.paymentRecords.getEntityBalance("João", user4.name);

        user2.paymentRecords.getEntityBalance("Jorge", user2.name);
        user1.paymentRecords.getEntityBalance("Jorge", user1.name);
        user3.paymentRecords.getEntityBalance("Jorge", user3.name);
        user4.paymentRecords.getEntityBalance("Jorge", user4.name);

        user2.paymentRecords.getEntityBalance("Tito", user2.name);
        user1.paymentRecords.getEntityBalance("Tito", user1.name);
        user3.paymentRecords.getEntityBalance("Tito", user3.name);
        user4.paymentRecords.getEntityBalance("Tito", user4.name);
        sleep(1000);
        user4.sendPaymentOffer(200, user3.getBean());
        sleep(1000);
        Entity user5 = new Entity("Felipe", 7852);
        sleep(1000);
        user5.sendHelloMessage();

        user2.paymentRecords.getEntityBalance("Felipe", user2.name);
        user1.paymentRecords.getEntityBalance("Felipe", user1.name);
        user3.paymentRecords.getEntityBalance("Felipe", user3.name);
        user4.paymentRecords.getEntityBalance("Felipe", user4.name);
        user5.paymentRecords.getEntityBalance("Felipe", user5.name);
        sleep(1000);
        user5.paymentRecords.getEntityBalance("Tito", user5.name);
        user5.paymentRecords.getEntityBalance("João", user5.name);
        user5.paymentRecords.getEntityBalance("Zé", user5.name);
        user5.paymentRecords.getEntityBalance("Jorge", user5.name);

        sleep(1000);
        user5.poolData();
        System.out.println(user5.entityPool.getEntityPoolString());
        user4.poolData();
        System.out.println(user4.entityPool.getEntityPoolString());
        user3.poolData();
        System.out.println(user3.entityPool.getEntityPoolString());
        user2.poolData();
        System.out.println(user2.entityPool.getEntityPoolString());
        user1.poolData();
        System.out.println(user1.entityPool.getEntityPoolString());
        //user3.sendExitMessage();
    }

    public static Entity criarUsuario() {
        Scanner read = new Scanner(System.in);
        System.out.println("Digite seu nome: ");
        String nome = read.nextLine();
        System.out.println("Digite a porta UDP para receber unicasts");
        int porta = read.nextInt();
        Entity user = new Entity(nome, porta);
        user.sendHelloMessage();
        return user;
    }

    public static void executarFuncao(Entity user) {
        System.out.println("Digite sua opção.");
        System.out.println("1) Enviar dinheiro para alguem.");
        System.out.println("2) Verificar seu EntityPool");
        System.out.println("3) Verificar seu PaymentRecords");
        System.out.println("4) Verificar sua carteira");
        System.out.println("0) Sair");
        Scanner read = new Scanner(System.in);
        int option;
        do {
            System.out.println("Digite sua opção:");
            option = read.nextInt();
            switch (option) {
                case 1:
                    System.out.println("Escolha o valor a ser enviado:");
                    int valor = read.nextInt();
                    System.out.println("Estes são os usuarios online:");
                    System.out.println(user.entityPool.getEntityPoolString());
                    System.out.println("Digite o nome de alguem para enviar o dinheiro:");
                    EntityBean payee = user.entityPool.typeTheEntity();
                    user.sendPaymentOffer(valor, payee);
                    break;
                case 2:
                    System.out.println("Estes são os usuarios online no seu EntityPool");
                    System.out.println(user.entityPool.getEntityPoolString());
                    break;
                case 3:
                    System.out.println("Estes são as transações registradas no seu PaymentRecords");
                    user.paymentRecords.printPaymentsContent();
                    break;
                case 4:
                    System.out.println("Verificando valores que tenho na carteira:");
                    user.paymentRecords.getEntityBalance(user.name, user.name);
                    break;
                case 0:
                    user.sendExitMessage();
                    break;
                default:
                    System.out.println("Opção inválida");
                    break;
            }
        } while (option != 0);
        user.sendExitMessage();
    }

}
