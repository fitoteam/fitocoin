package fitocoin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tiago Stapenhorst Martins
 * @author Felipe Ramon de Lara
 */
public class PaymentRecords implements Serializable {

    private List<Payment> records;

    public PaymentRecords() {
        this.records = new ArrayList<>();
    }

    public void addVerifiedPayment(Payment newPayment) {
        if (newPayment.getPaymentType() == Constants.PaymentType.NORMAL) {
            if (newPayment.isVerified()) { //newPayment.isVerified()
                this.records.add(newPayment);
            } else {
                System.out.println("Payment not verified.");
            }
        } else {
            if (newPayment.getPaymentType() == Constants.PaymentType.REWARD) {
                this.records.add(newPayment);
            }
        }
    }

    public List<Payment> getRecords() {
        return records;
    }

    public void setRecords(List<Payment> records) {
        this.records = records;
    }

    public String getPaymentsContent() {
        String output = "";
        output = output.concat("Payment Records array size:" + this.records.size());
        for (int i = 0; i < this.records.size(); i++) {
            output = output.concat("\n" + this.records.get(i).getPaymentsContent());
        }
        return output.concat("\nFim do Payment Records.");
    }

    public void printPaymentsContent() {
        System.out.println(this.getPaymentsContent());
    }

    public boolean payerHasEnoughFunds(Payment paymentToBeVerified) {
        int balance = Constants.INITIAL_MONEY;
        for (int i = 0; i < this.records.size(); i++) {
            if (this.records.get(i).getPayer().getName().equals(paymentToBeVerified.getPayer().getName())) {
                balance -= this.records.get(i).getValue();
            }
            if (this.records.get(i).getPayee().getName().equals(paymentToBeVerified.getPayer().getName())) {
                balance += this.records.get(i).getValue();
            }
        }
        if(balance >= (paymentToBeVerified.getValue() + Constants.MINING_REWARD)){
            System.out.println("SALDO SUFICIENTE - O saldo de " + paymentToBeVerified.getPayer().getName() + " é de: " + balance );
        } else {
            System.out.println("SALDO INSUFICIENTE - O saldo de " + paymentToBeVerified.getPayer().getName() + " é de: " + balance);
        }
        return balance >= (paymentToBeVerified.getValue() + Constants.MINING_REWARD);
    }

    public int getEntityBalance(String name, String entidade) {
        int balance = Constants.INITIAL_MONEY;
        int paymentsCount = 0;
        for (int i = 0; i < this.records.size(); i++) {
            if (this.records.get(i).getPayer().getName().equals(name)) {
                balance -= this.records.get(i).getValue();
                paymentsCount++;
            }
            if (this.records.get(i).getPayee().getName().equals(name)) {
                balance += this.records.get(i).getValue();
                paymentsCount++;
            }
        }
        System.out.println("De acordo com "+entidade+" o saldo de " + name + " é de: " + balance + ". Pagamentos relacionados encontrados: " + paymentsCount);
        return balance;
    }

    public boolean paymentAlreadyExist(Payment payment) {
        for (Payment record : this.records) {
            if (record.getTimeWhenCreated().equals(payment.getTimeWhenCreated()) && record.getValue() == payment.getValue()) {
                return true;
            }
        }
        return false;
    }

}
