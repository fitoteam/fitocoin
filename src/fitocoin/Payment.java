package fitocoin;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Tiago Stapenhorst Martins
 * @author Felipe Ramon de Lara
 */
public class Payment implements Serializable {

    private Date timeWhenVerifiedByMiner; //se foi verificado pelo mineirador ou não, se não foi, o valor é null, se foi, há o timestamp da hora da verificação.
    private Date timeWhenCreated;
    private EntityBean payee;
    private EntityBean payer;
    private EntityBean miner;
    private int value;
    private Constants.PaymentType paymentType; //se é um pagamento normal ou uma recompensa por mineiração

    public Payment(Date timeWhenVerifiedByMiner, Date timeWhenCreated, EntityBean payee, EntityBean payer, EntityBean miner, int value, Constants.PaymentType paymentType) {
        this.timeWhenVerifiedByMiner = timeWhenVerifiedByMiner;
        this.timeWhenCreated = timeWhenCreated;
        this.payee = payee;
        this.payer = payer;
        this.miner = miner;
        this.value = value;
        this.paymentType = paymentType;
    }

    public Date getTimeWhenCreated() {
        return timeWhenCreated;
    }

    public void setTimeWhenCreated(Date timeWhenCreated) {
        this.timeWhenCreated = timeWhenCreated;
    }

    public Date getTimeWhenVerifiedByMiner() {
        return timeWhenVerifiedByMiner;
    }

    public boolean isVerified() {
        return (this.timeWhenVerifiedByMiner != null);
    }

    public void setTimeWhenVerifiedByMiner(Date verified) {
        this.timeWhenVerifiedByMiner = verified;
    }

    public Constants.PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Constants.PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public EntityBean getPayee() {
        return payee;
    }

    public void setPayee(EntityBean payee) {
        this.payee = payee;
    }

    public EntityBean getPayer() {
        return payer;
    }

    public void setPayer(EntityBean payer) {
        this.payer = payer;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getPaymentsContent() {
        String out = "----------------";
        if (this.miner == null || this.timeWhenVerifiedByMiner == null) {
        } else {
            out = out.concat("Hora em que o minerador " + this.getMiner().getName() + " verificou: " + String.valueOf(this.timeWhenVerifiedByMiner));

        }
        out = out.concat("\nTransferencia de " + this.value + " de: " + this.payer.getName() + " para " + this.payee.getName());
        out = out.concat("\nTipo de pagamento: " + this.paymentType.toString());
        out = out.concat("\n----------------");
        return out;
    }

    public void setMiner(EntityBean bean) {
        this.miner = bean;
    }

    public EntityBean getMiner() {
        return miner;
    }
}
