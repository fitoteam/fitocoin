package fitocoin;

import java.io.IOException;
import static java.lang.Thread.sleep;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import org.apache.commons.lang3.SerializationUtils;

/**
 * @author Tiago Stapenhorst Martins
 * @author Felipe Ramon de Lara
 */
public final class Entity {

    protected KeyPair keys;
    protected SecretKey secretKey;
    protected InetAddress ipAddress;
    protected String name;
    protected int unicastPort;
    protected Thread unicastListener;
    protected Thread multicastListener;
    protected EntityPool entityPool;
    protected PaymentRecords paymentRecords; //our database

    //Init Entity and generate Public and Private Keys
    public Entity(String name, int port) {
        this.setIpAutomatically();
        this.name = name;
        this.unicastPort = port;
        this.entityPool = new EntityPool();
        this.paymentRecords = new PaymentRecords();
        this.setMulticastListener();
        this.setUnicastListener();

        try {
            //Inicializador da chave simétrica
            KeyGenerator keyGen;
            keyGen = KeyGenerator.getInstance(Constants.SYMMETRIC_CYPHER_ALGORITHM);
            keyGen.init(128);
            secretKey = keyGen.generateKey();

            //Inicializador da chave assimétrica
            KeyPairGenerator kpg = KeyPairGenerator.getInstance(Constants.ASYMMETRIC_CYPHER_ALGORITHM);
            kpg.initialize(Constants.CRYPTO_KEY_SIZE);
            this.keys = kpg.generateKeyPair();

            //Inicia os listeners, multicast e unicast
            this.startMulticastListener();
            this.startUnicastListener();
            
            //Mensagem envia seu EntityBean via multicast para que os usuarios o reconheçam na rede
            this.sendHelloMessage();
            System.out.println("Chave simétrica gerada para " + this.name + ":" + secretKey.toString());
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setIpAutomatically() {
        try {
            this.ipAddress = InetAddress.getLocalHost();
        } catch (UnknownHostException ex) {
            Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
            this.ipAddress = null;
        }
    }

    public void setUnicastListener() {
        this.unicastListener = new Thread() {
            @Override
            public void run() {
                this.setName("Unicast Listener");
                System.out.println("Unicast Listener Started.");
                try (DatagramSocket aSocket = new DatagramSocket(unicastPort)) {
                    // create socket at agreed port
                    byte[] buffer = new byte[Constants.BYTE_ARRAY_SIZE];
                    while (true) {
                        DatagramPacket request = new DatagramPacket(buffer, buffer.length);
                        aSocket.receive(request);
                        dealWithMessage((Message) SerializationUtils.deserialize(request.getData()));
                    }
                } catch (SocketException e) {
                    System.out.println("Socket: " + e.getMessage());
                } catch (IOException e) {
                    System.out.println("IO: " + e.getMessage());
                }
            }
        };
    }

    private void setMulticastListener() {
        this.multicastListener = new Thread() {
            @Override
            public void run() {
                this.setName("Multicast Listener");
                System.out.println("Multicast receiver n.o " + this.getId() + " started.");
                MulticastSocket s = null;
                try {
                    s = new MulticastSocket(Constants.MULTICAST_PORT);
                    s.joinGroup(Constants.MULTICAST_ADDRESS);
                    int count = 1;
                    do {
                        String output = "";
                        //Cria o buffer para guardar a mensagem recebida.
                        byte[] buffer = new byte[Constants.BYTE_ARRAY_SIZE];
                        DatagramPacket dp = new DatagramPacket(buffer, buffer.length);
                        s.receive(dp);
                        output = output.concat("Datagram " + count + " received from: " + dp.getSocketAddress());
                        //System.out.println(output);
                        //Deserialize object
                        Message message = (Message) SerializationUtils.deserialize(buffer);
                        dealWithMessage(message);
                        count++;
                    } while (!false //(messageIn.getData()).equals(("Exit").getBytes())
                            );
                    //s.leaveGroup(group);
                    //System.out.println("Receiver finished.");
                } catch (IOException ex) {
                    Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    s.close();
                }
            }
        };
    }

    public void dealWithMessage(Message message) {
        byte[] decryptedSecretKeyByte;
        SecretKey secretKey;
        byte[] decryptedOfferByte;
        Payment offer;

        if (this.getBean().getPkey().equals(message.getOrigin().getPkey())) {
            //Ignore message because it has been sent by the present Entity
            return;
        }
        switch (message.getType()) {
            case HELLO:
                /*
                1)Salva a EntityBean do rementente no EntityPool
                2)Envia seu PaymentRecords (banco de dados) ao remetente
                 */
                //1):
                entityPool.addEntity(message.getOrigin());
                System.out.println("Eu, " + this.name + " recebi uma atualização em minha EntityPool:" + entityPool.getEntityPoolString());
                //responder em unicast o proprio bean e PaymentRecords (update newbie)
                Message answer = new Message(Constants.MessageType.PAYMENT_RECORDS, this.getBean(), SerializationUtils.serialize(this.paymentRecords));
                sendMessageUnicast(message.getOrigin().getIp(), message.getOrigin().getUnicastPort(), answer);
                break;
            case PAYMENT_RECORDS:
                /*
                1) Adiciona o remetente da mensagem no EntityPool
                2) Verifica se o PaymentRecords está vazio
                3) Atualiza o seu PaymentRecords
                 */
                this.entityPool.addEntity(message.getOrigin());

                if (this.paymentRecords.getRecords().isEmpty()) {
                    //Deserializa PaymentRecords recebido pela mensagem e o substitui
                    this.paymentRecords = (PaymentRecords) SerializationUtils.deserialize(message.getBinaryObject());
                    String output = "";
                    output = output.concat("Eu, " + this.name + ", recebi " + Constants.MessageType.PAYMENT_RECORDS.toString() + " de " + message.getOrigin().getName() + "\n");
                    output = output.concat("Novo PaymentRecords:\n");
                    output = output.concat(this.paymentRecords.getPaymentsContent());
                    System.out.println(output);
                }
                break;
            case PAYMENT_OFFER:
                decryptedSecretKeyByte = Encryption.asymmetricDecryptMessage(message.getOrigin().getPkey(), message.getEncryptedSecretKey());
                secretKey = (SecretKey) SerializationUtils.deserialize(decryptedSecretKeyByte);
                System.out.println("Chave simétrica recebida:" + secretKey.toString());

                decryptedOfferByte = Encryption.symmetricDecrypt(message.getBinaryObject(), secretKey);
                offer = (Payment) SerializationUtils.deserialize(decryptedOfferByte);
                System.out.println("Eu, " + this.name + ", recebi " + Constants.MessageType.PAYMENT_OFFER.toString() + " no valor de " + offer.getValue() + " de " + message.getOrigin().getName() + "\n");

                //Pergunta ao usuário se ele aceita o pagamento ou não.
                //Se aceitar, envia uma mensagem VERIFY_PAYMENT em Multicast para que
                //as entidades do grupo verifiquem os fundos daquele pagamento.
                //Se negar, apenas não envia nada
                if (true) { //askForPaymentAccept()
                    //enviar pagamento com a criptografia do pagador
                    //entao mensagem deve ter bean do pagador
                    message.setType(Constants.MessageType.VERIFY_PAYMENT);
                    this.sendMessageMulticast(message);
                } else {
                    System.out.println("Você recusou o pagamento, ele foi descartado.");
                }
                break;

            case VERIFY_PAYMENT:
                /*
                Após receber um unicast oferecendo um pagamento, o receptor do pagamento
                envia em multicast uma solicitação para aquele pagamento seja verificado
                (se há dinheiro na conta ou não, etc...). Esta é a mensagem recebida.
                Então os que receberam por multicast irão minerar (verificar) esse pagamento
                e retornar se o pagamento foi validado para o receptor do pagamento.
                 */
                decryptedSecretKeyByte = Encryption.asymmetricDecryptMessage(message.getOrigin().getPkey(), message.getEncryptedSecretKey());
                secretKey = (SecretKey) SerializationUtils.deserialize(decryptedSecretKeyByte);
                decryptedOfferByte = Encryption.symmetricDecrypt(message.getBinaryObject(), secretKey);
                offer = (Payment) SerializationUtils.deserialize(decryptedOfferByte);
                //Se o pagamento estiver com esta entidade envolvida, ele não faz nada
                if (offer.getPayee().getName().equals(this.name) || offer.getPayer().getName().equals(this.name)) {
                    //Não faz nada, o pagamento veio ou é para esta entidade.
                } else if (this.paymentRecords.payerHasEnoughFunds(offer)) {
                    System.out.println("Eu, " + this.name + ", recebi " + Constants.MessageType.VERIFY_PAYMENT.toString() + " no valor de " + offer.getValue() + " de " + message.getOrigin().getName() + "\n");
                    Payment verifiedPayment = offer;
                    verifiedPayment.setTimeWhenVerifiedByMiner(new java.util.Date());
                    verifiedPayment.setMiner(this.getBean());
                    Message msg = new Message(Constants.MessageType.PAYMENT_VERIFIED, this.getBean(), SerializationUtils.serialize(verifiedPayment));
                    sendMessageUnicast(verifiedPayment.getPayer().getIp(), verifiedPayment.getPayee().getUnicastPort(), msg);
                } else {
                    //Não faz nada, nao existem fundos na carteira para a transacao.
                }
                break;
            case PAYMENT_VERIFIED:
                /*
                O usuario que aceitou o pagamento receberá esta mensagem via unicast.
                Ele verificara se aquele pagamento ja foi registrado em PaymentRecords.
                Se não tiver registrado, via multicast a ação de salvar aquele pagamento no PaymentRecords
                Salva no seu PaymentRecords o pagamento
                 */
                Payment verifiedPayment = (Payment) SerializationUtils.deserialize(message.getBinaryObject());
                if (this.paymentRecords.paymentAlreadyExist(verifiedPayment)) {
                    //Pagamento já existe, não faz nada
                } else {

                    //1) Adiciona payment ao payment records
                    //2) Notifica em multicast o novo pagamento
                    //3) Cria e adiciona premio do minerador ao payment records
                    //4) Notifica em multicast o prêmio do minerador
                    //1)
                    this.paymentRecords.addVerifiedPayment(verifiedPayment);
                    //2)
                    Message registerPayment = new Message(Constants.MessageType.RECORD_PAYMENT, this.getBean());
                    registerPayment.setBinaryObject(message.getBinaryObject());
                    System.out.println("Eu, " + this.name + ", recebi " + Constants.MessageType.PAYMENT_VERIFIED.toString() + " no valor de " + verifiedPayment.getValue() + " de " + message.getOrigin().getName() + "\n");

                    sendMessageMulticast(registerPayment);
                    //3)
                    Payment reward = new Payment(null, new java.util.Date(), verifiedPayment.getMiner(), verifiedPayment.getPayer(), null, Constants.MINING_REWARD, Constants.PaymentType.REWARD);

                    Message registerReward = new Message(Constants.MessageType.RECORD_PAYMENT, this.getBean());
                    registerReward.setBinaryObject(SerializationUtils.serialize(reward));
                    this.paymentRecords.addVerifiedPayment(reward);
                    //4)                    

                    sendMessageMulticast(registerReward);
                }

                break;

            case RECORD_PAYMENT:
                /*
                Salva o Payment recebido no PaymentRecord
                 */
                Payment payment = (Payment) SerializationUtils.deserialize(message.getBinaryObject());
                this.paymentRecords.addVerifiedPayment(payment);
                break;
            case GOODBYE:
                /*
                1) Remove o remetente da mensagem do EntityPool
                 */
                System.out.println("Eu, " + this.name + ", recebi " + Constants.MessageType.GOODBYE.toString() + " de " + message.getOrigin().getName() + ".\n");
                this.entityPool.removeEntity(message.getOrigin().getName());
                System.out.println(this.entityPool.getEntityPoolString());
                break;
            default:
                System.out.println("Tipo de mensagem desconhecida.");
                break;
        }
    }

    public void startMulticastListener() {
        this.multicastListener.start();
    }

    public void startUnicastListener() {
        this.unicastListener.start();
    }

    public void sendMessageUnicast(InetAddress ip, int port, Message message) {
        System.out.println("Unicast Sender Started.");
        DatagramSocket aSocket = null;
        try {
            aSocket = new DatagramSocket();
            byte[] m = SerializationUtils.serialize(message);
            DatagramPacket dp
                    = new DatagramPacket(m, m.length, ip, port);
            aSocket.send(dp);
        } catch (SocketException e) {
            System.out.println("Socket: " + e.getMessage());
        } catch (IOException e) {
            System.out.println("IO: " + e.getMessage());
        } finally {
            if (aSocket != null) {
                aSocket.close();
            }
        }
    }

    public boolean sendMessageMulticast(Message message) {
        MulticastSocket s = null;
        try {
            //System.out.println("Multicast Sender Started.");
            //Try to join multicast group and create socket.
            InetAddress group = InetAddress.getByName(Constants.MULTICAST_IP);
            s = new MulticastSocket(Constants.MULTICAST_PORT);
            s.joinGroup(group);
            //System.out.println("Success in joining multicast group:" + Constants.MULTICAST_IP + " port: " + Constants.MULTICAST_PORT);
            //System.out.println("Message to be sent:");
            message.getOrigin().getName();
            byte[] data = SerializationUtils.serialize(message);
            s.send(new DatagramPacket(data, data.length, group, Constants.MULTICAST_PORT));
            //System.out.println("Datagram sent to: " + group);
            s.leaveGroup(group);
            //System.out.println("Sender ended.");
        } catch (UnknownHostException ex) {
            Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return false;
    }

    public EntityBean getBean() {
        EntityBean eb = new EntityBean();
        eb.setIp(this.ipAddress);
        eb.setName(this.name);
        eb.setPkey(this.keys.getPublic());
        eb.setUnicastPort(this.unicastPort);
        return eb;
    }

    public void sendMyEntityBeanUnicast() {

    }

    public void sendExitMessage() {
        //Manda mensagem falando que está se desligando do grupo.
        //Pede para que seu EntityBean seja removido da EntityPool dos usuários do grupo
        Message byeMessage = new Message(Constants.MessageType.GOODBYE, getBean());
        sendMessageMulticast(byeMessage);
    }

    public void sendHelloMessage() {
        //Manda uma mensagem informando sua entrada no grupo, seu EntityBean e solicita o banco de dados atual de transações.
        Message newMessage = new Message(Constants.MessageType.HELLO, getBean());
        sendMessageMulticast(newMessage);
    }

    public void sendPaymentOffer(int value, EntityBean destination) {
        Payment paymentOffer = new Payment(null, new java.util.Date(), destination, this.getBean(), null, value, Constants.PaymentType.NORMAL);

        Message newMessage = new Message(Constants.MessageType.PAYMENT_OFFER, this.getBean());
        byte[] encriptedPayment = Encryption.symmetricEncrypt(SerializationUtils.serialize(paymentOffer), this.secretKey);
        newMessage.setBinaryObject(encriptedPayment);

        newMessage.setEncryptedSecretKey(Encryption.asymmetricEncryptMessage(this.keys.getPrivate(), SerializationUtils.serialize(this.secretKey)));

        sendMessageUnicast(destination.getIp(), destination.getUnicastPort(), newMessage);
    }

    public void acceptPaymentOffer(Message paymentMessage) {
        //Envia para multicast o pagamento para que os mineiradores o verifiquem
        paymentMessage.setType(Constants.MessageType.VERIFY_PAYMENT);
        sendMessageMulticast(paymentMessage);
    }

    private boolean askForPaymentAccept() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void poolData() {
        System.out.println("Eu, " + this.name + " possuo " + this.entityPool.getEntities().size() + " entidades registradas em meu pool no momento.");
    }

}
